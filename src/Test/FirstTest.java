package Test;

import org.apache.commons.math3.distribution.UniformIntegerDistribution;

/**
 * Created by jiyuanshi on 3/5/15.
 */
public class FirstTest {
    public static void main(String[] args) {
        UniformIntegerDistribution distribution = new UniformIntegerDistribution(0, 5);

        for (int i = 0; i < 20; i++) {
            System.out.println(distribution.sample());

        }
    }
}
