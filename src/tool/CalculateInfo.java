package tool;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

/**
 * Created by jiyuanshi on 3/5/15.
 */
public class CalculateInfo {
    public static void main(String[] args) throws IOException {
        try {
            CSVReader reader = new CSVReader(new FileReader("./vm_info"));
            CSVWriter resource_writer = new CSVWriter(new FileWriter("./resource_info"), CSVWriter.DEFAULT_SEPARATOR, CSVWriter.NO_QUOTE_CHARACTER);
            CSVWriter vm_num_writer = new CSVWriter(new FileWriter("./vm_num_info"), CSVWriter.DEFAULT_SEPARATOR, CSVWriter.NO_QUOTE_CHARACTER);
            List<String[]> items_list = reader.readAll();
            int[] vm_num_list = new int[170];
            double[] cpu_list = new double[170];
            double[] mem_list = new double[170];
            double[] disk_list = new double[170];

            for (String[] item_line : items_list) {

                int start_time = Integer.parseInt(item_line[1]);
                int end_time = Integer.parseInt(item_line[2]);
                double cpu = Double.parseDouble(item_line[3]);
                double mem = Double.parseDouble(item_line[4]);
                double disk = Double.parseDouble(item_line[5]);

                int start_hour = start_time / 60;
                int end_hour = end_time / 60;
                for (int i = start_hour; i <= end_hour; i++) {
                    vm_num_list[i]++;
                    cpu_list[i] = cpu_list[i] + cpu;
                    mem_list[i] = mem_list[i] + mem;
                    disk_list[i] = disk_list[i] + disk;
                }
            }

            for (int i = 0; i < 170; i++) {
                String[] vm_num = new String[2];
                String[] resource_info = new String[4];

                vm_num[0] = String.valueOf(i);
                vm_num[1] = String.valueOf(vm_num_list[i]);

                resource_info[0] = String.valueOf(i);
                resource_info[1] = String.valueOf(cpu_list[i]);
                resource_info[2] = String.valueOf(mem_list[i]);
                resource_info[3] = String.valueOf(disk_list[i]);

                vm_num_writer.writeNext(vm_num);
                resource_writer.writeNext(resource_info);

            }

            reader.close();
            resource_writer.flush();
            resource_writer.close();
            vm_num_writer.flush();
            vm_num_writer.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
