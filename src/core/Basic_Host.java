package core;

/**
 * This is the class for basic host using in Greedy FirstFit algorithm and Vectordot algorithm
 * <p/>
 * Created by stevenbush on 15/3/10.
 *
 * @version 1.0
 */
public class Basic_Host extends Abstract_Host {

    public Basic_Host(double[] host_conf) {
        super(host_conf);
    }

    /**
     * this method is used to add a vm to this host
     *
     * @param vm
     * @throws Exception
     */
    @Override
    public void add_vm(VM vm) throws Exception {
        vm_id_list.add(vm.getVm_id());
        vm.setDeployed_host(this);
        cpu_utilization = getCpu_utilization() + vm.getRelative_cpu_demand();
        mem_utilization = getMem_utilization() + vm.getRelative_mem_demand();
        disk_utilization = getDisk_utilization() + vm.getRelative_disk_demand();

        this.setMax_uitilization(Math.max(Math.max(getCpu_utilization(), getMem_utilization()), getDisk_utilization()));
    }

    /**
     * this method is used to delete a vm from this host
     *
     * @param vm
     */
    @Override
    public void delete_vm(VM vm) {
        vm_id_list.remove(vm.getVm_id());
        vm.emptyDeployed_host();
        cpu_utilization = getCpu_utilization() - vm.getRelative_cpu_demand();
        mem_utilization = getMem_utilization() - vm.getRelative_mem_demand();
        disk_utilization = getDisk_utilization() - vm.getRelative_disk_demand();

        this.setMax_uitilization(Math.max(Math.max(getCpu_utilization(), getMem_utilization()), getDisk_utilization()));

        if (this.vm_id_list.isEmpty()) {
            cpu_utilization = 0.0;
            mem_utilization = 0.0;
            disk_utilization = 0.0;
            max_uitilization = 0.0;
        }
    }
}
