package core;

/**
 * this class is used to represent a virtual machine
 * <p/>
 * Created by jiyuanshi on 3/8/15.
 *
 * @version 1.0
 */
public class VM {
    private String vm_id; // the id of this vm
    private double original_cpu_demand; // the original cpu resource demand
    private double original_mem_demand; // the original mem resource demand
    private double original_disk_demand; // the original disk resource demand
    private double relative_cpu_demand; // the relative cpu resource demand
    private double relative_mem_demand; // the relative mem resource demand
    private double relative_disk_demand; // the relative disk resource demand
    private double relative_max_demand; // the max resource demand of all resources
    private Abstract_Host deployed_host; // the host where this vm is deployed.

    public VM(String vm_id, double cpu_demand, double mem_demand, double disk_demand, double[] host_conf) {
        this.vm_id = vm_id;
        this.original_cpu_demand = cpu_demand;
        this.original_mem_demand = mem_demand;
        this.original_disk_demand = disk_demand;
        this.relative_cpu_demand = cpu_demand / host_conf[0];
        this.relative_mem_demand = mem_demand / host_conf[1];
        this.relative_disk_demand = disk_demand / host_conf[2];
        this.relative_max_demand = Math.max(Math.max(this.getRelative_cpu_demand(), this.getRelative_mem_demand()), this.getRelative_disk_demand());
    }

    public String getVm_id() {
        return vm_id;
    }

    public void setVm_id(String vm_id) {
        this.vm_id = vm_id;
    }

    public Abstract_Host getDeployed_host() {
        return deployed_host;
    }

    public void setDeployed_host(Abstract_Host deployed_host) {
        this.deployed_host = deployed_host;
    }

    /**
     * empty the deployed host of this vm
     */
    public void emptyDeployed_host() {
        this.deployed_host = null;
    }

    public double getOriginal_cpu_demand() {
        return original_cpu_demand;
    }

    public double getOriginal_mem_demand() {
        return original_mem_demand;
    }

    public double getOriginal_disk_demand() {
        return original_disk_demand;
    }

    public double getRelative_max_demand() {
        return relative_max_demand;
    }

    public double getRelative_cpu_demand() {
        return relative_cpu_demand;
    }

    public double getRelative_mem_demand() {
        return relative_mem_demand;
    }

    public double getRelative_disk_demand() {
        return relative_disk_demand;
    }
}
