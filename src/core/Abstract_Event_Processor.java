package core;

import java.util.ArrayList;
import java.util.Hashtable;

/**
 * This is a abstract event processor class
 * <p/>
 * Created by jiyuanshi on 3/8/15.
 *
 * @version V1.0
 */
public abstract class Abstract_Event_Processor {

    /**
     * the list used to store the host
     */
    public ArrayList<Abstract_Host> host_list;
    /**
     * the hash table used to store the vms
     */
    public Hashtable<String, VM> vm_table;
    /**
     * the Host capacity configuration
     */
    protected double[] host_conf;
    /**
     * the underload threshold
     */
    protected double underload_threshold;

    /**
     * the number of migrations
     */
    protected Integer migration_num;
    /**
     * the total number of vms
     */
    protected long total_vm_num;

    /**
     * the total cpu workload size
     */
    protected double total_cpu_workload_size;
    /**
     * the total mem workload size
     */
    protected double total_mem_workload_size;
    /**
     * the total disk workload size
     */
    protected double total_disk_workload_size;
    /**
     * the time to record the pre time
     */
    protected long pre_time;

    /**
     * this method is used to process the submit event
     *
     * @param event
     * @throws Exception
     */
    protected abstract void process_submit_event(String[] event, double[] resource_demands) throws Exception;

    /**
     * this method is used to process the finish event
     *
     * @param event
     * @throws Exception
     */
    protected abstract void process_finish_event(String[] event) throws Exception;

    /**
     * this function is used to process the incoming event
     *
     * @param event : the event string
     */
    public abstract void process(String[] event, double[] resource_demands);

    /**
     * the number of migrations
     */
    public Integer getMigration_num() {
        return migration_num;
    }

    /**
     * the total number of vms
     */
    public long getTotal_vm_num() {
        return total_vm_num;
    }

    /**
     * the total cpu workload size
     */
    public double getTotal_cpu_workload_size() {
        return total_cpu_workload_size;
    }

    /**
     * the total mem workload size
     */
    public double getTotal_mem_workload_size() {
        return total_mem_workload_size;
    }

    /**
     * the total disk workload size
     */
    public double getTotal_disk_workload_size() {
        return total_disk_workload_size;
    }

    /**
     * the Host capacity configuration
     */
    public double[] getHost_conf() {
        return host_conf;
    }
}
