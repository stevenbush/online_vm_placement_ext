package core;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.LinkedList;

/**
 * This class is used to process every event by using method in L.Eyraud-Dubois,H.Larcheveˆqueetal.,
 * "Optimizingresourceallocation while handling sla violations in cloud computing platforms," in IPDPS- 27th IEEE
 * International Parallel & Distributed Processing Symposium, 2013.
 * <p/>
 * Created by stevenbush on 15/3/10.
 *
 * @version 1.0
 */
public class ORA_Event_Processor extends Abstract_Event_Processor {
    /**
     * the Bins contain only T-items
     */
    private LinkedList<ORA_Host> TO_Bin_list;
    /**
     * the unfill T-bins
     */
    private LinkedList<ORA_Host> UT_Bin_list;
    /**
     * the Bins contain S-item
     */
    private LinkedList<ORA_Host> S_Bin_list;
    /**
     * the Bins contain only one S-item
     */
    private LinkedList<ORA_Host> US_Bin_list;
    /**
     * the Bins contain one S-item and some T-items
     */
    private LinkedList<ORA_Host> ST_Bin_list;

    public ORA_Event_Processor(double[] input_host_conf) {
        this.underload_threshold = 2.0 / 3.0;
        this.vm_table = new Hashtable<String, VM>();
        this.host_list = new ArrayList<Abstract_Host>();
        this.host_conf = new double[3];
        getHost_conf()[0] = input_host_conf[0];
        getHost_conf()[1] = input_host_conf[1];
        getHost_conf()[2] = input_host_conf[2];
        this.migration_num = 0;
        this.total_vm_num = (long) 0;
        this.TO_Bin_list = new LinkedList<ORA_Host>();
        this.UT_Bin_list = new LinkedList<ORA_Host>();
        this.S_Bin_list = new LinkedList<ORA_Host>();
        this.US_Bin_list = new LinkedList<ORA_Host>();
        this.ST_Bin_list = new LinkedList<ORA_Host>();
        this.total_cpu_workload_size = 0.0;
        this.total_disk_workload_size = 0.0;
        this.total_mem_workload_size = 0.0;
        this.pre_time = 0;
    }

    /**
     * remove the host from the related list
     *
     * @param host
     */
    private void remove_host(ORA_Host host) {
        this.host_list.remove(host);
        this.TO_Bin_list.remove(host);
        this.UT_Bin_list.remove(host);
        this.S_Bin_list.remove(host);
        this.US_Bin_list.remove(host);
        this.ST_Bin_list.remove(host);
    }

    /**
     * clear the bin type from the related list
     *
     * @param host
     */
    private void clear_bin_type(ORA_Host host) {
        this.TO_Bin_list.remove(host);
        this.UT_Bin_list.remove(host);
        this.S_Bin_list.remove(host);
        this.US_Bin_list.remove(host);
        this.ST_Bin_list.remove(host);
    }

    /**
     * update the bin type
     *
     * @param host
     */
    private void update_bin_type(ORA_Host host) {
        Integer B_num = host.getB_num();
        Integer L_num = host.getL_num();
        Integer S_num = host.getS_num();
        Integer T_num = host.getT_num();

        // first clear the host type
        this.TO_Bin_list.remove(host);
        this.UT_Bin_list.remove(host);
        this.S_Bin_list.remove(host);
        this.US_Bin_list.remove(host);
        this.ST_Bin_list.remove(host);

        if (T_num > 0 && S_num == 0 && L_num == 0 && B_num == 0) {
            TO_Bin_list.add(host);
            if (host.getMax_uitilization() <= 2.0 / 3.0) {
                UT_Bin_list.add(host);
            }
        }
        if (S_num > 0 && B_num == 0 && L_num == 0) {
            S_Bin_list.add(host);
        }
        if (S_num == 1 && T_num == 0 && L_num == 0 && B_num == 0) {
            US_Bin_list.add(host);
        }
        if (S_num > 0 && T_num > 0 && L_num == 0 && B_num == 0) {
            ST_Bin_list.add(host);
        }

        if (host.getVm_id_list().isEmpty()) {
            this.remove_host(host);
        }
    }

    /**
     * this method is used to insert a S-item
     *
     * @param vm
     */
    private void insert(VM vm) throws Exception {
        if (!US_Bin_list.isEmpty()) {
            // put this S-item into a exist US-bin
            ORA_Host host = US_Bin_list.get(0);
            vm.setDeployed_host(host);
            host.add_vm(vm);
            this.update_bin_type(host);
        } else {
            // open a new bin to accommodate this S-item
            ORA_Host new_host = new ORA_Host(getHost_conf());
            vm.setDeployed_host(new_host);
            new_host.add_vm(vm);
            this.update_bin_type(new_host);
            host_list.add(new_host);
        }
    }

    /**
     * move a vm_group from source host to the destination host
     *
     * @param src_host
     * @param des_host
     */
    private void move_group(ORA_Host src_host, ORA_Host des_host) throws Exception {
        ArrayList<String> candidate_group = src_host.get_group(vm_table).get(0);
        for (String vm_id : candidate_group) {
            VM current_vm = vm_table.get(vm_id);
            src_host.delete_vm(current_vm);
            current_vm.setDeployed_host(des_host);
            des_host.add_vm(current_vm);
        }

        if (src_host.getVm_id_list().isEmpty()) {
            this.remove_host(src_host);
        }
    }

    /**
     * this method is used to fill a L-bin
     *
     * @param host
     */
    private void fill(ORA_Host host) throws Exception {
        while ((host.getMax_uitilization() <= 2.0 / 3.0)
                && ((!this.TO_Bin_list.isEmpty()) || (!this.ST_Bin_list.isEmpty()))) {
            if (!this.UT_Bin_list.isEmpty()) {
                ORA_Host current_bin = this.UT_Bin_list.get(0);
                this.move_group(current_bin, host);
                this.update_bin_type(current_bin);
                this.migration_num = this.migration_num + 1;
            } else {
                if (!this.TO_Bin_list.isEmpty()) {
                    ORA_Host current_bin = this.TO_Bin_list.get(0);
                    this.move_group(current_bin, host);
                    this.update_bin_type(current_bin);
                    this.migration_num = this.migration_num + 1;
                } else if (!this.ST_Bin_list.isEmpty()) {
                    ORA_Host current_bin = this.ST_Bin_list.get(0);
                    this.move_group(current_bin, host);
                    this.update_bin_type(current_bin);
                    this.migration_num = this.migration_num + 1;
                }
            }
        }
    }

    /**
     * this method is used to merge two UT-bin
     *
     * @param host1
     * @param host2
     */
    private void merge(ORA_Host host1, ORA_Host host2) throws Exception {
        ORA_Host max_host;
        ORA_Host min_host;
        if (host1.getMax_uitilization() < host2.getMax_uitilization()) {
            min_host = host1;
            max_host = host2;
        } else {
            min_host = host2;
            max_host = host1;
        }
        while (min_host.getMax_uitilization() > 0 && max_host.getMax_uitilization() <= this.underload_threshold) {
            this.move_group(min_host, max_host);
            this.migration_num = this.migration_num + 1;
        }
    }

    /**
     * this method is used to handle a underload host
     *
     * @param host
     */
    private void underload_handler(ORA_Host host) throws Exception {
        Integer L_num = 0;
        Integer S_num = 0;
        VM S_item = null;
        for (String id : host.getVm_id_list()) {
            VM current_vm = vm_table.get(id);
            if (current_vm.getRelative_max_demand() <= 2.0 / 3.0 && current_vm.getRelative_max_demand() > 1.0 / 2.0) { // L-item
                L_num = L_num + 1;
            }
            if (current_vm.getRelative_max_demand() <= 1.0 / 2.0 && current_vm.getRelative_max_demand() > 1.0 / 3.0) { // S-item
                S_num = S_num + 1;
                S_item = current_vm;
            }
        }

        if (L_num > 0) {
            this.fill(host);
            this.update_bin_type(host);
        } else {
            if (S_num > 0) {
                host.delete_vm(S_item);
                // this.update_bin_type(host);
                this.insert(S_item);
                this.migration_num = this.migration_num + 1;
            }
            if (!host.getVm_id_list().isEmpty() && !UT_Bin_list.isEmpty()) {
                if (!UT_Bin_list.contains(host)) {
                    ORA_Host UT_host = UT_Bin_list.getFirst();
                    this.merge(host, UT_host);
                    this.update_bin_type(UT_host);
                    this.update_bin_type(host);
                    if (UT_host.getVm_id_list().isEmpty()) {
                        this.remove_host(UT_host);
                    }
                }
            }
        }
        if (host.getVm_id_list().isEmpty()) {
            this.remove_host(host);
        }
    }

    /**
     * this method is used to process the submit event
     *
     * @param event
     * @param resource_demands
     * @throws Exception
     */
    @Override
    protected void process_submit_event(String[] event, double[] resource_demands) throws Exception {
        this.migration_num = 0;
        String vm_id = event[1];
        if (!vm_table.containsKey(vm_id)) {
            this.total_vm_num = this.getTotal_vm_num() + 1;
            VM new_vm = new VM(vm_id, resource_demands[0], resource_demands[1], resource_demands[2], getHost_conf());
            vm_table.put(vm_id, new_vm);

            if (new_vm.getRelative_max_demand() > (2.0 / 3.0)) { // B-item
                // open a new bin to accommodate this B-item
                ORA_Host new_host = new ORA_Host(getHost_conf());
                new_vm.setDeployed_host(new_host);
                new_host.add_vm(new_vm);
                host_list.add(new_host);
            } else if (new_vm.getRelative_max_demand() <= (2.0 / 3.0) && new_vm.getRelative_max_demand() > 1.0 / 2.0) { // L-item
                // open a new bin to accommodate this L-item and fill this L-bin
                ORA_Host new_host = new ORA_Host(getHost_conf());
                new_vm.setDeployed_host(new_host);
                new_host.add_vm(new_vm);
                host_list.add(new_host);
                fill(new_host);
                this.update_bin_type(new_host);
            } else if (new_vm.getRelative_max_demand() <= 1.0 / 2.0 && new_vm.getRelative_max_demand() > 1.0 / 3.0) { // S-item
                this.insert(new_vm);
            } else { // T-item
                if (!UT_Bin_list.isEmpty()) {
                    // put this T-item into the exist UT-bin
                    ORA_Host host = UT_Bin_list.get(0);
                    new_vm.setDeployed_host(host);
                    host.add_vm(new_vm);
                    this.update_bin_type(host);
                } else {
                    // open a new T-bin to accommodate this T-item
                    ORA_Host new_host = new ORA_Host(getHost_conf());
                    new_vm.setDeployed_host(new_host);
                    new_host.add_vm(new_vm);
                    host_list.add(new_host);
                    this.update_bin_type(new_host);
                }
            }

            this.total_cpu_workload_size = this.getTotal_cpu_workload_size() + resource_demands[0];
            this.total_mem_workload_size = this.getTotal_mem_workload_size() + resource_demands[1];
            this.total_disk_workload_size = this.getTotal_disk_workload_size() + resource_demands[2];
        }
    }

    /**
     * this method is used to process the finish event
     *
     * @param event
     * @throws Exception
     */
    @Override
    protected void process_finish_event(String[] event) throws Exception {
        this.migration_num = 0;
        String vm_id = event[1];
        if (vm_table.containsKey(vm_id)) {
            VM finish_vm = vm_table.get(vm_id);
            ORA_Host deployed_host = (ORA_Host) finish_vm.getDeployed_host();
            deployed_host.delete_vm(finish_vm);

            // if after deletion of vm, this host become empty, just remove this host from host list
            if (deployed_host.getVm_id_list().isEmpty()) {
                this.remove_host(deployed_host);
            } else {
                // this.update_bin_type(deployed_host);
                if (deployed_host.getMax_uitilization() <= this.underload_threshold) {
                    this.clear_bin_type(deployed_host);
                    underload_handler(deployed_host);
                }
                this.update_bin_type(deployed_host);
                if (deployed_host.getVm_id_list().isEmpty()) {
                    this.remove_host(deployed_host);
                }
            }
            this.total_cpu_workload_size = this.getTotal_cpu_workload_size() - finish_vm.getOriginal_cpu_demand();
            this.total_mem_workload_size = this.getTotal_mem_workload_size() - finish_vm.getOriginal_mem_demand();
            this.total_disk_workload_size = this.getTotal_disk_workload_size() - finish_vm.getOriginal_disk_demand();
            vm_table.remove(vm_id);
        }
    }

    /**
     * this function is used to process the incoming event
     *
     * @param event            : the event string
     * @param resource_demands
     */
    @Override
    public void process(String[] event, double[] resource_demands) {
        String event_type = event[2];

        if (event_type.equals("1")) {
            try {
                process_submit_event(event, resource_demands);
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        if (event_type.equals("2")) {
            try {
                process_finish_event(event);
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }
}
