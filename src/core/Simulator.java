package core;

import au.com.bytecode.opencsv.CSVReader;
import org.apache.commons.math3.distribution.UniformIntegerDistribution;

import java.io.*;
import java.util.*;

/**
 * This is the main class to perform the simulator
 * <p/>
 * Created by stevenbush on 15/3/9.
 *
 * @version 1.0
 */
public class Simulator {
    public static void main(String[] args) {
        if (args.length < 4) {
            System.out.println("No input file, resultpath, algorithm name, result name specified.");
            System.out.println("please enter \"simulator inputfile resultpath algorithm_name result_name\"");
            return;
        }

        if (!args[2].equals("GFF") && !args[2].equals("ORA") && !args[2].equals("ROBP") && !args[2].equals("VD") && !args[2].equals("ROBP_R") && !args[2].equals("ORA_R")) {
            System.out.println("the input algorithm name is wrong, please input the correct algorithm name: (GFF, " + "ORA, ROBP, VD, ORA_R, ROBP_R)");
            return;
        }

        File input_event_file = new File(args[0] + "/event_info");
        File input_vm_file = new File(args[0] + "vm_info");
        File result_path = new File(args[1]);
        String algorithm_name = args[2];
        String result_name = args[3];

        System.out.println("inputeventfile: " + input_event_file.getName());
        System.out.println("inputvmfile: " + input_vm_file.getName());
        System.out.println("resultpath: " + result_path.getName());
        System.out.println("algorithm_name: " + algorithm_name);
        System.out.println("result_name: " + result_name);

        try {
            System.out.println("Start Simulation...");

            // read the workload event information from file
            CSVReader event_reader = new CSVReader(new FileReader(input_event_file));
            List<String[]> events_list = event_reader.readAll();

            // read the VM information of the workload
            CSVReader vm_reader = new CSVReader(new FileReader(input_vm_file));
            List<String[]> vms_list = vm_reader.readAll();

            // file writer to record the result log
            FileWriter result_log_fw = new FileWriter(result_path + "/" + result_name + "_" + algorithm_name +
                    "_resultlog.csv");
            BufferedWriter result_log_writer = new BufferedWriter(result_log_fw);

            // file writer to record the resource result log
            FileWriter res_result_log_fw = new FileWriter(result_path + "/" + result_name + "_" + algorithm_name +
                    "_resource_resultlog.csv");
            BufferedWriter res_result_log_writer = new BufferedWriter(res_result_log_fw);

            // file writer to record the host log
            FileWriter host_fw = new FileWriter(result_path + "/" + result_name + "_" + algorithm_name + "_hostlog" +
                    ".csv");
            BufferedWriter host_writer = new BufferedWriter(host_fw);

            Hashtable<String, double[]> vm_storage = new Hashtable<String, double[]>();
            for (String[] vm_item : vms_list) {
                String vm_id = vm_item[0];
                double[] demand_list = new double[3];
                demand_list[0] = Double.valueOf(vm_item[3]);
                demand_list[1] = Double.valueOf(vm_item[4]);
                demand_list[2] = Double.valueOf(vm_item[5]);
                vm_storage.put(vm_id, demand_list);
            }

            int[] vm_event_processor_array = new int[vms_list.size() + 1];

            double[][] host_conf_lists = {{1.0, 1.5, 2.0}, {1.0, 2.0, 1.5}, {1.5, 1.0, 2.0}, {1.5, 2.0, 1.0}, {2.0, 1.0, 1.5}, {2.0, 1.5, 1.0}};

            Abstract_Event_Processor[] event_processor_array = new Abstract_Event_Processor[6];

            for (int i = 0; i < event_processor_array.length; i++) {

                if (algorithm_name.equals("ROBP") || algorithm_name.equals("ROBP_R")) {
                    event_processor_array[i] = new ROBP_Event_Processor(host_conf_lists[i]);
                } else if (algorithm_name.equals("ORA") || algorithm_name.equals("ORA_R")) {
                    event_processor_array[i] = new ORA_Event_Processor(host_conf_lists[i]);
                } else if (algorithm_name.equals("GFF")) {
                    event_processor_array[i] = new GreedyFirstFit_Event_Processor(host_conf_lists[i]);
                } else if (algorithm_name.equals("VD")) {
                    event_processor_array[i] = new VectorDot_Event_Processor(host_conf_lists[i]);
                } else {
                    System.out.println("no algorithm specified.");
                    result_log_writer.flush();
                    result_log_writer.close();
                    res_result_log_writer.flush();
                    res_result_log_writer.close();
                    host_writer.flush();
                    host_writer.close();
                    return;
                }
            }

            for (String[] event : events_list) {
                System.out.println("processing time: " + event[0] + ", vm_id: " + event[1] + ", event_type: " +
                        event[2]);
                String vm_id = event[1];
                String event_type = event[2];
                double[] vm_demand = vm_storage.get(vm_id);

                double vectordot = 0.0;
                UniformIntegerDistribution distribution = new UniformIntegerDistribution(0, 5);
                int chosen_index = 0;

                if (event_type.equals("1")) {
                    if (algorithm_name.equals("ORA") || algorithm_name.equals("ROBP")) {
                        for (int i = 0; i < event_processor_array.length; i++) {
                            double current_vectordot = vm_demand[0] * event_processor_array[i].getHost_conf()[0] + vm_demand[1] * event_processor_array[i].getHost_conf()[1] + vm_demand[2] * event_processor_array[i].getHost_conf()[2];
                            if (current_vectordot > vectordot) {
                                vectordot = current_vectordot;
                                chosen_index = i;
                            }
                        }
                    } else {
                        chosen_index = distribution.sample();
                    }
                    vm_event_processor_array[Integer.valueOf(vm_id)] = chosen_index;
                } else {
                    chosen_index = vm_event_processor_array[Integer.valueOf(vm_id)];
                }

                Long begin_time = System.nanoTime();
                String event_time = event[0];
                event_processor_array[chosen_index].process(event, vm_demand);
                Long end_time = System.nanoTime();
                Long time_overhead = end_time - begin_time;

                double migration_num = 0, host_num = 0, current_vm_num = 0, total_vm_num = 0, total_cpu_workload_size = 0, total_mem_workload_size = 0, total_disk_workload_size = 0;

                for (int i = 0; i < event_processor_array.length; i++) {
                    migration_num = migration_num + event_processor_array[i].getMigration_num();
                    host_num = host_num + event_processor_array[i].host_list.size();
                    current_vm_num = current_vm_num + event_processor_array[i].vm_table.size();
                    total_vm_num = total_vm_num + event_processor_array[i].getTotal_vm_num();
                    total_cpu_workload_size = total_cpu_workload_size + event_processor_array[i].getTotal_cpu_workload_size();
                    total_mem_workload_size = total_mem_workload_size + event_processor_array[i].getTotal_mem_workload_size();
                    total_disk_workload_size = total_disk_workload_size + event_processor_array[i].getTotal_disk_workload_size();
                }

                // event_time+time_overhead+migrations_number+host_number+current_vm_number+total_vm_number+cpu_workload+mem_workload+disk_workload
                String log_string = event_time + "," + String.valueOf(time_overhead) + "," + String.valueOf(migration_num) + "," + String.valueOf(host_num) + "," + String.valueOf(current_vm_num) + "," + String.valueOf(total_vm_num) + "," + String.valueOf(total_cpu_workload_size) + "," + String.valueOf(total_mem_workload_size) + "," + String.valueOf(total_disk_workload_size);

                // record result log information
                try {
                    result_log_writer.write(log_string);
                    result_log_writer.newLine();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                double real_total_cpu_workload_size = 0;
                double real_total_mem_workload_size = 0;
                double real_total_disk_workload_size = 0;

                for (int i = 0; i < event_processor_array.length; i++) {
                    for (int j = 0; j < event_processor_array[i].host_list.size(); j++) {
                        Abstract_Host host = event_processor_array[i].host_list.get(j);
                        real_total_cpu_workload_size = real_total_cpu_workload_size + host.getCpu_capacity();
                        real_total_mem_workload_size = real_total_mem_workload_size + host.getMem_capacity();
                        real_total_disk_workload_size = real_total_disk_workload_size + host.getDisk_capacity();
                    }
                }

                String res_log_string = event_time + "," + real_total_cpu_workload_size + "," + real_total_mem_workload_size + "," + real_total_disk_workload_size + "," + total_cpu_workload_size + "," + total_mem_workload_size + "," + total_disk_workload_size;

                // record resource result log information
                try {
                    res_result_log_writer.write(res_log_string);
                    res_result_log_writer.newLine();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

            event_reader.close();
            vm_reader.close();

            result_log_writer.flush();
            result_log_writer.close();
            res_result_log_writer.flush();
            res_result_log_writer.close();
            host_writer.flush();
            host_writer.close();

            System.out.println("Finish Simulation...");
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

}
