package core;

import java.util.ArrayList;
import java.util.Hashtable;

/**
 * this class is used to represent a physic host using by my paper
 * <p/>
 * Created by stevenbush on 15/3/9.
 *
 * @version 1.0
 */
public class ROBP_Host extends Abstract_Host {

    /**
     * the threshold of group operation
     */
    private Double group_threshold;
    /**
     * these vms that can be grouped
     */
    private ArrayList<String> vms_canbe_grouped;
    /**
     * the number of H_items
     */
    private Integer H_num;
    /**
     * the number of B_items
     */
    private Integer B_num;
    /**
     * the number of L_items
     */
    private Integer L_num;
    /**
     * the number of S_items
     */
    private Integer S_num;
    /**
     * the number of T_items
     */
    private Integer T_num;
    /**
     * the number of M_items
     */
    private Integer M_num;

    public ROBP_Host(double[] host_conf) {
        super(host_conf);
        this.group_threshold = 1.0 / 4.0;
        this.vms_canbe_grouped = new ArrayList<String>();
        this.H_num = 0;
        this.B_num = 0;
        this.L_num = 0;
        this.S_num = 0;
        this.T_num = 0;
        this.M_num = 0;
    }

    /**
     * this method is used to add a vm to this host, and if this vm can be group then put it into the corresponding list
     *
     * @param vm
     * @throws Exception
     */
    @Override
    public void add_vm(VM vm) throws Exception {
        vm_id_list.add(vm.getVm_id());
        vm.setDeployed_host(this);
        cpu_utilization = getCpu_utilization() + vm.getRelative_cpu_demand();
        mem_utilization = getMem_utilization() + vm.getRelative_mem_demand();
        disk_utilization = getDisk_utilization() + vm.getRelative_disk_demand();

        this.setMax_uitilization(Math.max(Math.max(getCpu_utilization(), getMem_utilization()), getDisk_utilization()));

        // if this vm canbe grouped, them put it into the corresponding list
        if (vm.getRelative_max_demand() <= this.group_threshold) {
            this.vms_canbe_grouped.add(vm.getVm_id());
        }

        // update the item type counter
        if (vm.getRelative_max_demand() <= 1.0 && vm.getRelative_max_demand() > 3.0 / 4.0) { // H-item
            this.H_num = this.H_num + 1;
        }
        if (vm.getRelative_max_demand() <= 3.0 / 4.0 && vm.getRelative_max_demand() > 2.0 / 3.0) { // B-item
            this.B_num = this.B_num + 1;
        }
        if (vm.getRelative_max_demand() <= 2.0 / 3.0 && vm.getRelative_max_demand() > 1.0 / 2.0) { // L-item
            this.L_num = this.L_num + 1;
        }
        if (vm.getRelative_max_demand() <= 1.0 / 2.0 && vm.getRelative_max_demand() > 1.0 / 3.0) { // S-item
            this.S_num = this.S_num + 1;
        }
        if (vm.getRelative_max_demand() <= 1.0 / 3.0 && vm.getRelative_max_demand() > 1.0 / 4.0) { // T-item
            this.T_num = this.T_num + 1;
        }
        if (vm.getRelative_max_demand() <= 1.0 / 4.0 && vm.getRelative_max_demand() > 0) { // M-item
            this.M_num = this.M_num + 1;
        }
    }

    /**
     * this method is used to delete a vm from this host, and if this vm can be group then delete it from the
     * corresponding list
     *
     * @param vm
     */
    @Override
    public void delete_vm(VM vm) {
        vm_id_list.remove(vm.getVm_id());
        vm.emptyDeployed_host();
        cpu_utilization = getCpu_utilization() - vm.getRelative_cpu_demand();
        mem_utilization = getMem_utilization() - vm.getRelative_mem_demand();
        disk_utilization = getDisk_utilization() - vm.getRelative_disk_demand();

        this.setMax_uitilization(Math.max(Math.max(getCpu_utilization(), getMem_utilization()), getDisk_utilization()));

        // if this vm can be grouped, them remove it from the corresponding list
        if (this.vms_canbe_grouped.contains(vm.getVm_id())) {
            this.vms_canbe_grouped.remove(vm.getVm_id());
        }

        // update the item type counter
        if (vm.getRelative_max_demand() <= 1.0 && vm.getRelative_max_demand() > 3.0 / 4.0) { // H-item
            this.H_num = this.H_num - 1;
        }
        if (vm.getRelative_max_demand() <= 3.0 / 4.0 && vm.getRelative_max_demand() > 2.0 / 3.0) { // B-item
            this.B_num = this.B_num - 1;
        }
        if (vm.getRelative_max_demand() <= 2.0 / 3.0 && vm.getRelative_max_demand() > 1.0 / 2.0) { // L-item
            this.L_num = this.L_num - 1;
        }
        if (vm.getRelative_max_demand() <= 1.0 / 2.0 && vm.getRelative_max_demand() > 1.0 / 3.0) { // S-item
            this.S_num = this.S_num - 1;
        }
        if (vm.getRelative_max_demand() <= 1.0 / 3.0 && vm.getRelative_max_demand() > 1.0 / 4.0) { // T-item
            this.T_num = this.T_num - 1;
        }
        if (vm.getRelative_max_demand() <= 1.0 / 4.0 && vm.getRelative_max_demand() > 0) { // M-item
            this.M_num = this.M_num - 1;
        }

        if (this.vm_id_list.isEmpty()) {
            cpu_utilization = 0.0;
            mem_utilization = 0.0;
            disk_utilization = 0.0;
            max_uitilization = 0.0;
            this.H_num = 0;
            this.B_num = 0;
            this.L_num = 0;
            this.S_num = 0;
            this.T_num = 0;
            this.M_num = 0;
            this.vms_canbe_grouped.clear();
        }

    }

    /**
     * get the group list in this host
     *
     * @param vm_table
     * @return the group list
     */
    public ArrayList<ArrayList<String>> get_group(Hashtable<String, VM> vm_table) {
        ArrayList<ArrayList<String>> group_list = new ArrayList<ArrayList<String>>();
        ArrayList<String> tmp_vm_list = (ArrayList<String>) this.vms_canbe_grouped.clone();
        while (!tmp_vm_list.isEmpty()) {
            ArrayList<String> group = new ArrayList<String>();
            double group_size = 0.0;
            for (String vm_id : tmp_vm_list) {
                VM current_vm = vm_table.get(vm_id);
                if (current_vm.getRelative_max_demand() + group_size <= group_threshold) {
                    group.add(vm_id);
                    group_size = group_size + current_vm.getRelative_max_demand();
                }
            }
            tmp_vm_list.removeAll(group);
            group_list.add(group);
        }
        return group_list;
    }

    /**
     * the threshold of group operation
     */
    public Double getGroup_threshold() {
        return group_threshold;
    }

    public void setGroup_threshold(Double group_threshold) {
        this.group_threshold = group_threshold;
    }

    /**
     * these vms that can be grouped
     */
    public ArrayList<String> getVms_canbe_grouped() {
        return vms_canbe_grouped;
    }

    public void setVms_canbe_grouped(ArrayList<String> vms_canbe_grouped) {
        this.vms_canbe_grouped = vms_canbe_grouped;
    }

    /**
     * the number of H_items
     */
    public Integer getH_num() {
        return H_num;
    }

    public void setH_num(Integer h_num) {
        H_num = h_num;
    }

    /**
     * the number of B_items
     */
    public Integer getB_num() {
        return B_num;
    }

    public void setB_num(Integer b_num) {
        B_num = b_num;
    }

    /**
     * the number of L_items
     */
    public Integer getL_num() {
        return L_num;
    }

    public void setL_num(Integer l_num) {
        L_num = l_num;
    }

    /**
     * the number of S_items
     */
    public Integer getS_num() {
        return S_num;
    }

    public void setS_num(Integer s_num) {
        S_num = s_num;
    }

    /**
     * the number of T_items
     */
    public Integer getT_num() {
        return T_num;
    }

    public void setT_num(Integer t_num) {
        T_num = t_num;
    }

    /**
     * the number of M_items
     */
    public Integer getM_num() {
        return M_num;
    }

    public void setM_num(Integer m_num) {
        M_num = m_num;
    }
}
