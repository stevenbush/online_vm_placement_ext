package core;

import java.util.ArrayList;

/**
 * this is a abstract class used to represent a physic host
 * <p/>
 * Created by jiyuanshi on 3/8/15.
 *
 * @version V1.0
 */
public abstract class Abstract_Host {
    /**
     * the utilization of cpu resource
     */
    protected double cpu_utilization;
    /**
     * the utilization of mem resource
     */
    protected double mem_utilization;
    /**
     * the utilization of disk resource
     */
    protected double disk_utilization;
    /**
     * the max resource utilization for two dimensional situation
     */
    protected double max_uitilization;
    /**
     * the capacity of cpu resource
     */
    protected double cpu_capacity;
    /**
     * the capacity of mem resource
     */
    protected double mem_capacity;
    /**
     * the capacity of disk resource
     */
    protected double disk_capacity;
    /**
     * the id of vm putted in this host
     */
    protected ArrayList<String> vm_id_list;

    public Abstract_Host(double[] host_conf) {
        this.cpu_utilization = 0.0;
        this.mem_utilization = 0.0;
        this.disk_utilization = 0.0;
        this.setCpu_capacity(host_conf[0]); //cpu capacity
        this.setMem_capacity(host_conf[1]); //mem capacity
        this.setDisk_capacity(host_conf[2]); //disk capacity
        this.setVm_id_list(new ArrayList<String>());
    }

    /**
     * this method is used to add a vm to this host
     *
     * @param vm
     * @throws Exception
     */
    public abstract void add_vm(VM vm) throws Exception;

    /**
     * this method is used to delete a vm from this host
     *
     * @param vm
     */
    public abstract void delete_vm(VM vm);

    /**
     * the capacity of cpu resource
     */
    public double getCpu_capacity() {
        return cpu_capacity;
    }

    public void setCpu_capacity(double cpu_capacity) {
        this.cpu_capacity = cpu_capacity;
    }

    /**
     * the capacity of mem resource
     */
    public double getMem_capacity() {
        return mem_capacity;
    }

    public void setMem_capacity(double mem_capacity) {
        this.mem_capacity = mem_capacity;
    }

    /**
     * the capacity of disk resource
     */
    public double getDisk_capacity() {
        return disk_capacity;
    }

    public void setDisk_capacity(double disk_capacity) {
        this.disk_capacity = disk_capacity;
    }

    /**
     * the id of vm putted in this host
     */
    public ArrayList<String> getVm_id_list() {
        return vm_id_list;
    }

    public void setVm_id_list(ArrayList<String> vm_id_list) {
        this.vm_id_list = vm_id_list;
    }

    /**
     * the max resource utilization for two dimensional situation
     */
    /**
     * the max resource utilization for two dimensional situation
     */
    public double getMax_uitilization() {
        return max_uitilization;
    }

    public void setMax_uitilization(double max_uitilization) {
        this.max_uitilization = max_uitilization;
    }

    /**
     * the utilization of cpu resource
     */
    public double getCpu_utilization() {
        return cpu_utilization;
    }

    /**
     * the utilization of mem resource
     */
    public double getMem_utilization() {
        return mem_utilization;
    }

    /**
     * the utilization of disk resource
     */
    public double getDisk_utilization() {
        return disk_utilization;
    }
}
