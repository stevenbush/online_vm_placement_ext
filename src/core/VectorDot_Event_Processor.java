package core;

import java.util.ArrayList;
import java.util.Hashtable;

/**
 * This class is used to process every event by using the VectorDot method
 * <p/>
 * Created by stevenbush on 15/3/10.
 *
 * @version 1.0
 */
public class VectorDot_Event_Processor extends Abstract_Event_Processor {

    public VectorDot_Event_Processor(double[] input_host_conf) {
        this.vm_table = new Hashtable<String, VM>();
        this.host_list = new ArrayList<Abstract_Host>();
        this.host_conf = new double[3];
        getHost_conf()[0] = input_host_conf[0];
        getHost_conf()[1] = input_host_conf[1];
        getHost_conf()[2] = input_host_conf[2];
        this.migration_num = 0;
        this.total_vm_num = (long) 0;
        this.total_cpu_workload_size = 0.0;
        this.total_disk_workload_size = 0.0;
        this.total_mem_workload_size = 0.0;
        this.pre_time = 0;
    }

    /**
     * use best fit method to put a vm into a host
     *
     * @param vm
     */
    private void put_vm(VM vm) throws Exception {
        Boolean find_host = false;
        Basic_Host target_host = null;
        double vectordot = Double.MAX_VALUE;
        for (int i = 0; i < host_list.size(); i++) {
            Basic_Host current_host = (Basic_Host) host_list.get(i);
            if ((vm.getRelative_cpu_demand() + current_host.getCpu_utilization() <= 1.0) && (vm.getRelative_mem_demand() + current_host.getMem_utilization() <= 1.0) && (vm.getRelative_disk_demand() + current_host.getDisk_utilization() <= 1.0)) {
                find_host = true;
                double current_vectordot = vm.getRelative_cpu_demand() * current_host.getCpu_utilization() + vm.getRelative_mem_demand() * current_host.getMem_utilization() + vm.getRelative_disk_demand() + current_host.getDisk_utilization();
                if (current_vectordot < vectordot) {
                    target_host = current_host;
                }
            }
        }
        // if can not find a exist host to accept the new vm, we open a new host to accept this new vm
        if (!find_host) {
            Basic_Host new_host = new Basic_Host(getHost_conf());
            vm.setDeployed_host(new_host);
            new_host.add_vm(vm);
            host_list.add(new_host);
        } else {
            vm.setDeployed_host(target_host);
            target_host.add_vm(vm);
        }

    }

    /**
     * this method is used to process the submit event
     *
     * @param event
     * @param resource_demands
     * @throws Exception
     */
    @Override
    protected void process_submit_event(String[] event, double[] resource_demands) throws Exception {
        this.migration_num = 0;
        String vm_id = event[1];
        if (!vm_table.containsKey(vm_id)) {
            this.total_vm_num = this.getTotal_vm_num() + 1;
            VM new_vm = new VM(vm_id, resource_demands[0], resource_demands[1], resource_demands[2], getHost_conf());
            vm_table.put(vm_id, new_vm);
            this.put_vm(new_vm);
            this.total_cpu_workload_size = this.getTotal_cpu_workload_size() + resource_demands[0];
            this.total_mem_workload_size = this.getTotal_mem_workload_size() + resource_demands[1];
            this.total_disk_workload_size = this.getTotal_disk_workload_size() + resource_demands[2];
        }
    }

    /**
     * this method is used to process the finish event
     *
     * @param event
     * @throws Exception
     */
    @Override
    protected void process_finish_event(String[] event) throws Exception {
        this.migration_num = 0;
        String vm_id = event[1];
        if (vm_table.containsKey(vm_id)) {
            VM finish_vm = vm_table.get(vm_id);
            Basic_Host deployed_host = (Basic_Host) finish_vm.getDeployed_host();
            deployed_host.delete_vm(finish_vm);

            // if after deletion of vm, this host become empty, just remove this host from host list
            if (deployed_host.getVm_id_list().isEmpty()) {
                host_list.remove(deployed_host);
            }

            this.total_cpu_workload_size = this.getTotal_cpu_workload_size() - finish_vm.getOriginal_cpu_demand();
            this.total_mem_workload_size = this.getTotal_mem_workload_size() - finish_vm.getOriginal_mem_demand();
            this.total_disk_workload_size = this.getTotal_disk_workload_size() - finish_vm.getOriginal_disk_demand();
            vm_table.remove(vm_id);
        }
    }

    /**
     * this function is used to process the incoming event
     *
     * @param event            : the event string
     * @param resource_demands
     */
    @Override
    public void process(String[] event, double[] resource_demands) {
        String event_type = event[2];

        if (event_type.equals("1")) {
            try {
                process_submit_event(event, resource_demands);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (event_type.equals("2")) {
            try {
                process_finish_event(event);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
