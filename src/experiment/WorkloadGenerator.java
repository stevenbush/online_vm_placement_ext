package experiment;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;
import org.apache.commons.math3.distribution.ExponentialDistribution;
import org.apache.commons.math3.distribution.UniformIntegerDistribution;
import org.apache.commons.math3.distribution.UniformRealDistribution;
import org.apache.commons.math3.random.UniformRandomGenerator;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

/**
 * Created by jiyuanshi on 3/4/15.
 */
public class WorkloadGenerator {
    public static void main(String[] args) {
        try {
            CSVWriter vminfo_writer = new CSVWriter(new FileWriter("./vm_info"), CSVWriter.DEFAULT_SEPARATOR, CSVWriter.NO_QUOTE_CHARACTER);
            CSVWriter eventinfo_writer = new CSVWriter(new FileWriter("./event_info"), CSVWriter.DEFAULT_SEPARATOR, CSVWriter.NO_QUOTE_CHARACTER);

            ExponentialDistribution starttimeDistribution = new ExponentialDistribution(2);
            UniformIntegerDistribution runtimeDistribution = new UniformIntegerDistribution(60, 120);
            UniformRealDistribution demandDistribution = new UniformRealDistribution(0.0, 1.0);

            TreeMap<Integer, ArrayList<String[]>> treeMap = new TreeMap<Integer, ArrayList<String[]>>();

            System.out.println("Start to generate workload data");

            // Generate the VM information
            int vm_id = 1;
            int start_time = 0;
            while (true) {
                System.out.println("Generating VM " + vm_id);
                String[] outitem = new String[6];
                int end_time = start_time + runtimeDistribution.sample();
                double cpu = Math.round(demandDistribution.sample() * 1000) * 1.0 / 1000;
                while (cpu <= 0.001) {
                    cpu = Math.round(demandDistribution.sample() * 1000) * 1.0 / 1000;
                }
                double mem = Math.round(demandDistribution.sample() * 1000) * 1.0 / 1000;
                while (mem <= 0.001) {
                    mem = Math.round(demandDistribution.sample() * 1000) * 1.0 / 1000;
                }
                double disk = Math.round(demandDistribution.sample() * 1000) * 1.0 / 1000;
                while (disk <= 0.001) {
                    disk = Math.round(demandDistribution.sample() * 1000) * 1.0 / 1000;
                }

                // vm_id+start_time+end_time+cpu+mem+disk
                outitem[0] = String.valueOf(vm_id);
                outitem[1] = String.valueOf(start_time);
                outitem[2] = String.valueOf(end_time);
                outitem[3] = String.valueOf(cpu);
                outitem[4] = String.valueOf(mem);
                outitem[5] = String.valueOf(disk);

                vminfo_writer.writeNext(outitem);

                System.out.println("Generating Events");
                // generate event information: time+vm_id+type, type: 1 is start, 2 is end
                // adding start event
                if (!treeMap.containsKey(start_time)) {
                    String[] event = new String[3];
                    event[0] = String.valueOf(start_time);//time
                    event[1] = String.valueOf(vm_id);//vm_id
                    event[2] = "1";//type
                    ArrayList<String[]> strList = new ArrayList<String[]>();
                    strList.add(event);
                    treeMap.put(start_time, strList);

                } else {
                    String[] event = new String[3];
                    event[0] = String.valueOf(start_time);//time
                    event[1] = String.valueOf(vm_id);//vm_id
                    event[2] = "1";//type
                    treeMap.get(start_time).add(event);
                }

                //adding end event
                if (!treeMap.containsKey(end_time)) {
                    String[] event = new String[3];
                    event[0] = String.valueOf(end_time);//time
                    event[1] = String.valueOf(vm_id);//vm_id
                    event[2] = "2";//type
                    ArrayList<String[]> strList = new ArrayList<String[]>();
                    strList.add(event);
                    treeMap.put(end_time, strList);

                } else {
                    String[] event = new String[3];
                    event[0] = String.valueOf(end_time);//time
                    event[1] = String.valueOf(vm_id);//vm_id
                    event[2] = "2";//type
                    treeMap.get(end_time).add(event);
                }

                vm_id++;
                start_time = (int) (start_time + Math.round(starttimeDistribution.sample()));
                if (start_time >= 10080) {
                    break;
                }
            }

            // generate event information
            Iterator iterator = treeMap.entrySet().iterator();
            while (iterator.hasNext()) {
                Map.Entry ent = (Map.Entry) iterator.next();
                System.out.println(ent.getKey());
                ArrayList<String[]> event_list = (ArrayList<String[]>) ent.getValue();
                for (String[] strs : event_list) {
                    eventinfo_writer.writeNext(strs);
                }
            }

            vminfo_writer.flush();
            vminfo_writer.close();
            eventinfo_writer.flush();
            eventinfo_writer.close();
            System.out.println("Generating Complete");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
