package experiment;

import au.com.bytecode.opencsv.CSVWriter;
import org.apache.commons.math3.distribution.ExponentialDistribution;
import org.apache.commons.math3.distribution.UniformIntegerDistribution;
import org.apache.commons.math3.distribution.UniformRealDistribution;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by stevenbush on 15/3/10.
 */
public class Workload_Specified_Generator {
    public static void main(String[] args) {
        try {
            CSVWriter vminfo_writer = new CSVWriter(new FileWriter("./vm_info"), CSVWriter.DEFAULT_SEPARATOR, CSVWriter.NO_QUOTE_CHARACTER);
            CSVWriter eventinfo_writer = new CSVWriter(new FileWriter("./event_info"), CSVWriter.DEFAULT_SEPARATOR, CSVWriter.NO_QUOTE_CHARACTER);

            ExponentialDistribution starttimeDistribution = new ExponentialDistribution(2);
            UniformIntegerDistribution runtimeDistribution = new UniformIntegerDistribution(1440, 4320);
            UniformRealDistribution tiny_seed_distribution = new UniformRealDistribution(0, 0.25);
            UniformRealDistribution normal_seed_distribution = new UniformRealDistribution(0.25, 1.0);

            double ratio = 0.5; // this is the ratio of different resource demands dimension

            TreeMap<Integer, ArrayList<String[]>> treeMap = new TreeMap<Integer, ArrayList<String[]>>();

            System.out.println("Start to generate workload data");

            // Generate the VM information
            int vm_id = 1;
            int start_time = 0;
            double tiny_size = 0.0;
            while (true) {
                System.out.println("Generating VM " + vm_id);
                String[] outitem = new String[6];
                int end_time = start_time + runtimeDistribution.sample();

                double cpu, mem, disk, seed_value;

                if (tiny_size <= 0.5) {
                    seed_value = Math.round(tiny_seed_distribution.sample() * 1000) * 1.0 / 1000;
                    while (seed_value <= 0.001) {
                        seed_value = Math.round(tiny_seed_distribution.sample() * 1000) * 1.0 / 1000;
                    }
                    double upper_bound = Math.min(1 / seed_value, 1 / ratio);
                    double lower_bound = ratio;
                    UniformRealDistribution scalling_distribution = new UniformRealDistribution(lower_bound, upper_bound);

                    cpu = Math.round(seed_value * 1000) * 1.0 / 1000;
                    mem = Math.round(seed_value * scalling_distribution.sample() * 1000) * 1.0 / 1000;
                    disk = Math.round(seed_value * scalling_distribution.sample() * 1000) * 1.0 / 1000;
                    tiny_size = tiny_size + Math.max(Math.max(cpu, mem), disk);
                } else {
                    seed_value = Math.round(normal_seed_distribution.sample() * 1000) * 1.0 / 1000;
                    while (seed_value <= 0.001) {
                        seed_value = Math.round(tiny_seed_distribution.sample() * 1000) * 1.0 / 1000;
                    }
                    double upper_bound = Math.min(1 / seed_value, 1 / ratio);
                    double lower_bound = ratio;
                    UniformRealDistribution scalling_distribution = new UniformRealDistribution(lower_bound, upper_bound);

                    cpu = Math.round(seed_value * 1000) * 1.0 / 1000;
                    mem = Math.round(seed_value * scalling_distribution.sample() * 1000) * 1.0 / 1000;
                    disk = Math.round(seed_value * scalling_distribution.sample() * 1000) * 1.0 / 1000;
                    tiny_size = 0.0;
                }

                // vm_id+start_time+end_time+cpu+mem+disk
                outitem[0] = String.valueOf(vm_id);
                outitem[1] = String.valueOf(start_time);
                outitem[2] = String.valueOf(end_time);
                outitem[3] = String.valueOf(cpu);
                outitem[4] = String.valueOf(mem);
                outitem[5] = String.valueOf(disk);

                vminfo_writer.writeNext(outitem);

                System.out.println("Generating Events");
                // generate event information: time+vm_id+type, type: 1 is start, 2 is end
                // adding start event
                if (!treeMap.containsKey(start_time)) {
                    String[] event = new String[3];
                    event[0] = String.valueOf(start_time);//time
                    event[1] = String.valueOf(vm_id);//vm_id
                    event[2] = "1";//type
                    ArrayList<String[]> strList = new ArrayList<String[]>();
                    strList.add(event);
                    treeMap.put(start_time, strList);

                } else {
                    String[] event = new String[3];
                    event[0] = String.valueOf(start_time);//time
                    event[1] = String.valueOf(vm_id);//vm_id
                    event[2] = "1";//type
                    treeMap.get(start_time).add(event);
                }

                //adding end event
                if (!treeMap.containsKey(end_time)) {
                    String[] event = new String[3];
                    event[0] = String.valueOf(end_time);//time
                    event[1] = String.valueOf(vm_id);//vm_id
                    event[2] = "2";//type
                    ArrayList<String[]> strList = new ArrayList<String[]>();
                    strList.add(event);
                    treeMap.put(end_time, strList);

                } else {
                    String[] event = new String[3];
                    event[0] = String.valueOf(end_time);//time
                    event[1] = String.valueOf(vm_id);//vm_id
                    event[2] = "2";//type
                    treeMap.get(end_time).add(event);
                }

                vm_id++;
                start_time = (int) (start_time + Math.round(starttimeDistribution.sample()));
                if (start_time >= 10080) {
                    break;
                }
            }

            // generate event information
            Iterator iterator = treeMap.entrySet().iterator();
            while (iterator.hasNext()) {
                Map.Entry ent = (Map.Entry) iterator.next();
                System.out.println(ent.getKey());
                ArrayList<String[]> event_list = (ArrayList<String[]>) ent.getValue();
                for (String[] strs : event_list) {
                    eventinfo_writer.writeNext(strs);
                }
            }

            vminfo_writer.flush();
            vminfo_writer.close();
            eventinfo_writer.flush();
            eventinfo_writer.close();
            System.out.println("Generating Complete");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
